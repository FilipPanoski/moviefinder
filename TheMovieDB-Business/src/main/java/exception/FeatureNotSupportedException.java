package exception;

/**
 * Exception that is thrown when the user tries to use a feature that is not supported in themoviedb.org api.
 */
public class FeatureNotSupportedException extends RuntimeException {

    /**
     * Generates an exception and prints the reason why it was thrown.
     * @param message the message that needs to be printed.
     */
    public FeatureNotSupportedException(String message) {
        super(message);
    }
}

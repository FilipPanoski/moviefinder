package exception;

/**
 * Exception that is thrown when a themoviedb.org movie contains a release date that can not be parsed to
 * a java.util.Calendar date.
 */
public class DateParseException extends RuntimeException {

    /**
     * Generates the exception and prints the message why it was caused and
     * the original exception that caused it.
     *
     * @param message the message that is printed when the exception is thrown.
     * @param ex the original exception that caused this exception to be thrown.
     */
    public DateParseException(String message, Throwable ex) {
        super(message, ex);
    }
}

package object;

import java.util.HashMap;
import java.util.Map;

/**
 * Enumeration which holds themoviedb.org genres and their corresponding ids.
 */
public enum TheMovieDbGenre {
    Action(28),
    Adventure(12),
    Comedy(35),
    Horror(27),
    SciFi(878),
    Thriller(53);

    private int id;
    private static Map<Integer, TheMovieDbGenre> map = new HashMap<>();

    TheMovieDbGenre(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    /**
     * Puts all genres into a map from which you can search by themoviedb.org genre id.
     */
    static {
        for (TheMovieDbGenre genre: TheMovieDbGenre.values()) {
            map.put(genre.id, genre);
        }
    }

    /**
     * Searches for a genre by themoviedb.org genre id.
     *
     * @param id is the unique themoviedb.org id for a genre.
     * @return the corresponding TheMovieDbGenre.class genre.
     */
    public static TheMovieDbGenre getGenre(Integer id) {
        return map.get(id);
    }
}

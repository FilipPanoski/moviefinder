package service;

import object.Genre;
import object.Movie;
import org.bson.json.JsonParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import service.helper.MovieDbMapperService;

import java.io.IOException;
import java.util.*;

/**
 * Implementation of SearchService interface which looks for movies in themoviedb.org api.
 */
@Service
public class MovieDbSearchServiceImpl implements SearchMovieService {

    private RestTemplate restTemplate;
    private String moviedbUrl;
    private MovieDbMapperService movieDbMapperService;

    @Autowired
    public MovieDbSearchServiceImpl(String searchUrl, MovieDbMapperService movieDbMapperService) {
        this.restTemplate = new RestTemplate();
        this.moviedbUrl = searchUrl;
        this.movieDbMapperService = movieDbMapperService;
    }

    /**
     * {@inheritDoc}
     *
     * Doesn't provide a search for genres because themoviedb.org search api doesn't support it.
     */
    public List<Movie> searchMovies(String movieName, List<Genre> genres) {
        Map query = new HashMap<String, String>();
        Optional.ofNullable(movieName).ifPresent(value -> query.put("name", movieName));
        String restResult = restTemplate.getForObject(moviedbUrl, String.class, query);
        try {
            return movieDbMapperService.mapToMovieList(restResult);
        } catch (IOException ex) {
            throw new JsonParseException("Json failed to parse.", ex);
        }
    }
}

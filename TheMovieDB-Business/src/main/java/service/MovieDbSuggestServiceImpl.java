package service;

import object.Genre;
import object.Movie;
import object.TheMovieDbGenre;
import org.bson.json.JsonParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import service.helper.MovieDbMapperService;

import java.io.IOException;
import java.util.*;

/**
 * Implementation of SuggestMovieService interface which suggests a random movie from themoviedb.org api.
 */
@Service
public class MovieDbSuggestServiceImpl implements SuggestMovieService {

    private RestTemplate restTemplate;
    private String moviedbUrl;
    private MovieDbMapperService movieDbMapperService;

    @Autowired
    public MovieDbSuggestServiceImpl(String suggestUrl, MovieDbMapperService movieDbMapperService) {
        this.restTemplate = new RestTemplate();
        this.moviedbUrl = suggestUrl;
        this.movieDbMapperService = movieDbMapperService;
    }

    /**
     * {@inheritDoc}
     *
     * Searches for a movie in a pool of movies with the exact movie rating or above that rating.
     */
    public Movie suggestRandomMovie(Integer movieRating, List<Genre> genres) {
        List<Movie> movieList;
        String restResult = restTemplate.getForObject(moviedbUrl, String.class, makeQuery(movieRating, genres));
        try {
            movieList = movieDbMapperService.mapToMovieList(restResult);
        } catch (IOException ex) {
            throw new JsonParseException("Json failed to parse.", ex);
        }
        return getRandomMovie(movieList);
    }

    private Map<String, String> makeQuery(Integer movieRating, List<Genre> genres) {
        Map query = new HashMap<String, String>();
        query.put("rating", "");
        query.put("genres", "");
        Optional.ofNullable(movieRating).ifPresent(value -> query.put("rating", movieRating));
        Optional.ofNullable(genres).ifPresent(value -> query.put("genres", value.stream()
                        .map(genre -> TheMovieDbGenre.valueOf(genre.toString()).getId()).toArray()));
        return query;
    }

    private Movie getRandomMovie(List<Movie> qualifiedMovies) {
        if (qualifiedMovies.size() == 0) {
            return null;
        }
        int randomIndex = new Random().nextInt(qualifiedMovies.size());
        return qualifiedMovies.get(randomIndex);
    }
}

package service;

import exception.FeatureNotSupportedException;
import object.Movie;
import org.springframework.stereotype.Service;
import service.AddMovieService;


@Service
public class MovieDbAddServiceImpl implements AddMovieService {

    public Movie addNewMovieToDatabase(Movie movie, String releaseDate) {
        throw new FeatureNotSupportedException("This feature is not supported.");
    }
}

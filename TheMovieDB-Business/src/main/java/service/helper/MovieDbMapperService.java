package service.helper;

import object.Movie;
import org.bson.json.JsonParseException;

import java.io.IOException;
import java.util.List;

/**
 * Service which maps themoviedb.org api json to the relevant moviefinder objects.
 */
public interface MovieDbMapperService {

    /**
     * Maps themoviedb.org json response to a list of movies.
     * Throws an exception if the mapping can not be properly done.
     *
     * @param jsonString the string containing the whole json response.
     * @return the list of movies.
     * @throws JsonParseException
     * @throws IOException
     */
    List<Movie> mapToMovieList(String jsonString) throws JsonParseException, IOException;
}

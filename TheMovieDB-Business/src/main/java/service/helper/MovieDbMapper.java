package service.helper;

import object.Movie;
import object.TheMovieDbJsonResult;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper interface that maps TheMovieDbJsonResult.class objects to object.Movie.class using MapStruct.
 */
@Mapper(componentModel = "spring")
public interface MovieDbMapper {

    /**
     * Maps a TheMovieDbJsonResult.class object to a object.Movie.class object.
     * The new movie has a mapped name and rating.
     * The release date and genres are not mapped(other services do these tasks).
     *
     * @param jsonResult an object of the class representation of themoviedb.org api json response.
     * @return a movie with the corresponding name and rating.
     */
    @Mapping(source = "title", target = "name")
    @Mapping(source = "voteAverage", target = "rating")
    @Mapping(source = "releaseDate", target = "releaseDate", ignore = true)
    Movie jsonResultToMovie(TheMovieDbJsonResult jsonResult);
}

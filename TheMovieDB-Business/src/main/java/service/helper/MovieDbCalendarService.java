package service.helper;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Service which manipulates with java.util.Calendar dates.
 */
public interface MovieDbCalendarService {

    /**
     * Parses the String argument date into a java.util.Calendar date.
     * Throws an exception if the String can not be parsed.
     *
     * @param date the String that needs to be parsed.
     * @return a parsed java.util.Calendar date.
     * @throws ParseException
     */
    Calendar parseStringDateToCalendar(String date) throws ParseException;
}

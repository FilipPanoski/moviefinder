package service.helper.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.helper.MovieDbCalendarService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Implementation of the MovieDbCalendarService interface which helps with java.util.Calendar manipulation.
 */
@Service
public class MovieDbCalendarServiceImpl implements MovieDbCalendarService {

    private String dateFormat;

    @Autowired
    public MovieDbCalendarServiceImpl(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    /**
     * {@inheritDoc}
     *
     * Additionally, returns null if some movies do not have a release date in themoviedb.org api
     */
    public Calendar parseStringDateToCalendar(String date) throws ParseException {
        if (date.equals("")) {
            return null;
        }
        Calendar parsedDate = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        parsedDate.setTime(simpleDateFormat.parse(date));
        return parsedDate;
    }
}

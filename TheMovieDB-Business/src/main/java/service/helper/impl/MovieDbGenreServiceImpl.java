package service.helper.impl;

import object.Genre;
import object.TheMovieDbGenre;
import org.springframework.stereotype.Service;
import service.helper.MovieDbGenreService;

import java.util.*;
import java.util.stream.Collectors;


/**
 * Implementation of the MovieDbGenreService which parses between TheMovieDbGenre.class and object.Genre.
 */
@Service
public class MovieDbGenreServiceImpl implements MovieDbGenreService {

    /**
     * {@inheritDoc}
     */
    public List<Genre> parseJsonGenreToGenreList(List<Integer> jsonGenres) {
        List<TheMovieDbGenre> movieDbGenres =  jsonGenres.stream()
                .map(id -> TheMovieDbGenre.getGenre(id)).collect(Collectors.toList());
        movieDbGenres.removeAll(Collections.singleton(null));

        List<Genre> genres =  movieDbGenres.stream().map(genre -> Genre.valueOf(genre.toString()))
                .collect(Collectors.toList());
        return genres;
    }
}

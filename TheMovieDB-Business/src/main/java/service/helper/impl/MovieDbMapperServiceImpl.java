package service.helper.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import exception.DateParseException;
import object.*;
import org.bson.json.JsonParseException;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.helper.MovieDbCalendarService;
import service.helper.MovieDbGenreService;
import service.helper.MovieDbMapper;
import service.helper.MovieDbMapperService;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Implementation of MovieDbMapperService which helps with mapping themoviedb.org json format into moviefinder objects.
 */
@Service
public class MovieDbMapperServiceImpl implements MovieDbMapperService {

    private MovieDbMapper mapper;
    private MovieDbCalendarService movieDbCalendarService;
    private MovieDbGenreService movieDbGenreService;

    @Autowired
    public MovieDbMapperServiceImpl(MovieDbCalendarService movieDbCalendarService,
                                    MovieDbGenreService movieDbGenreService) {

        this.mapper = Mappers.getMapper(MovieDbMapper.class);
        this.movieDbCalendarService = movieDbCalendarService;
        this.movieDbGenreService = movieDbGenreService;
    }

    /**
     * {@inheritDoc}
     */
    public List<Movie> mapToMovieList(String jsonString) throws JsonParseException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        TheMovieDbJson json = objectMapper.readValue(jsonString, TheMovieDbJson.class);
        List<TheMovieDbJsonResult> results = json.getResults();
        return putJsonMoviesIntoMovieList(results);
    }

    private List<Movie> putJsonMoviesIntoMovieList(List<TheMovieDbJsonResult> results) {
        List<Movie> movieList = new ArrayList<>();
        for (TheMovieDbJsonResult result: results) {
            Movie movie = mapper.jsonResultToMovie(result);
            movie.setReleaseDate(tryToParseStringDateToCalendar(result.getReleaseDate()));
            movie.setGenre(movieDbGenreService.parseJsonGenreToGenreList(result.getGenreIds()));
            movieList.add(movie);
        }
        return movieList;
    }

    private Calendar tryToParseStringDateToCalendar(String date) {
        try {
            return movieDbCalendarService.parseStringDateToCalendar(date);
        } catch (ParseException ex) {
            throw new DateParseException("Date failed to parse.", ex);
        }
    }
}

package service.helper;

import object.Genre;

import java.util.List;

/**
 * Service which helps with parsing themoviedb.org json format genres into object.Genre.class genres.
 */
public interface MovieDbGenreService {

    /**
     * Parses the id of the genres from the jsonGenre argument to the corresponding object.Genre.class genre.
     *
     * @param jsonGenre the list containing the ids of the genres in the json format.
     * @return a list of object.Genre.class genres.
     */
    List<Genre> parseJsonGenreToGenreList(List<Integer> jsonGenre);
}

package config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import service.*;
import service.helper.*;
import service.helper.impl.MovieDbCalendarServiceImpl;
import service.helper.impl.MovieDbGenreServiceImpl;
import service.helper.impl.MovieDbMapperServiceImpl;

/**
 * Configures beans of all services in TheMovieDb-Business module.
 */
@Profile("themoviedb")
@Configuration
public class TheMovieDbBusinessConfig {

    @Bean
    public AddMovieService addMovieService() {
        return new MovieDbAddServiceImpl();
    }

    /**
     * @param searchUrl the url for themoviedb.org search api.
     * @param movieDbMapperService service which helps with mapping the json response to relevant moviefinder objects.
     */
   @Bean
   public SearchMovieService searchMovieService(@Value("${themoviedb.searchUrl}") String searchUrl,
                                                     MovieDbMapperService movieDbMapperService) {

       return new MovieDbSearchServiceImpl(searchUrl, movieDbMapperService);
    }

    /**
     * @param suggestUrl the url for themoviedb.org discover api.
     * @param movieDbMapperService service which helps with mapping the json response to relevant moviefinder objects.
     */
    @Bean
    public SuggestMovieService suggestMovieService(@Value("${themoviedb.suggestUrl}") String suggestUrl,
                                                   MovieDbMapperService movieDbMapperService) {

        return new MovieDbSuggestServiceImpl(suggestUrl, movieDbMapperService);
    }

    /**
     * @param movieDbCalendarService service which helps with parsing json release dates to java.util.Calendar dates.
     * @param movieDbGenreService service which helps with parsing json genre ids to relevant object.Genre genres.
     */
    @Bean
    public MovieDbMapperService movieDbMapperService(MovieDbCalendarService movieDbCalendarService,
                                                         MovieDbGenreService movieDbGenreService) {

        return new MovieDbMapperServiceImpl(movieDbCalendarService, movieDbGenreService);
    }

    /**
     * @param dateFormat the needed format to parse a themoviedb.org date to a java.util.Calendar date.
     */
    @Bean
    public MovieDbCalendarService movieDbCalendarService(@Value("${themoviedb.dateFormat}") String dateFormat) {
        return new MovieDbCalendarServiceImpl(dateFormat);
    }

    @Bean
    public MovieDbGenreService movieDbGenreService() {
        return new MovieDbGenreServiceImpl();
    }
}

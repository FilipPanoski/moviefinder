import object.Movie;
import object.TheMovieDbJsonResult;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import service.helper.MovieDbMapper;

@RunWith(SpringRunner.class)
public class MovieDbMapperTest {

    private MovieDbMapper mapper;

    @Before
    public void setUp() throws Exception {
        mapper = Mappers.getMapper(service.helper.MovieDbMapper.class);
    }

    @Test
    public void testMovieDbMapper() throws Exception {
        TheMovieDbJsonResult result = new TheMovieDbJsonResult();
        result.setTitle("someName");
        result.setVoteAverage(7.0);
        result.setReleaseDate("2012-12-12");
        Movie movie = mapper.jsonResultToMovie(result);

        assertEquals(result.getTitle(), movie.getName());
        assertEquals(null, movie.getReleaseDate());
        assertTrue(result.getVoteAverage().intValue() == movie.getRating());
    }
}

import static org.junit.Assert.*;

import object.Genre;
import object.Movie;
import org.bson.json.JsonParseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

import org.springframework.test.context.junit4.SpringRunner;
import service.MovieDbSuggestServiceImpl;
import service.SuggestMovieService;
import service.helper.MovieDbGenreService;
import service.helper.MovieDbMapperService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

@RunWith(SpringRunner.class)
public class MovieDbSuggestServiceImplTest {

    @Mock
    private MovieDbMapperService movieDbMapperService;
    @Mock
    private MovieDbGenreService movieDbGenreService;
    private SuggestMovieService suggestMovieService;
    private String movieDbUrl;

    @Before
    public void setUp() throws Exception {
        movieDbUrl = "https://api.themoviedb.org/3/discover/movie?api_key=964e1a2c4054adb70ac02a6842ca8a9c" +
                "&vote_average.gte={rating}&with_genres={genres}";
        suggestMovieService = new MovieDbSuggestServiceImpl(movieDbUrl, movieDbMapperService);
    }

    @Test
    public void testSuggestRandomMovieMethodWhenParseIsSuccessful() throws Exception {
        Movie mockMovieFirst = new Movie("someName", Calendar.getInstance(),
                10, Arrays.asList(Genre.Action));
        Movie mockMovieSecond = new Movie("someOtherName", Calendar.getInstance(),
                10, Arrays.asList(Genre.Action));
        List<Movie> mockMovieList = new ArrayList<>();
        mockMovieList.add(mockMovieFirst);
        mockMovieList.add(mockMovieSecond);
        Integer[] mockGenreIds = new Integer[1];
        mockGenreIds[0] = 28;
        when(movieDbMapperService.mapToMovieList(anyString())).thenReturn(mockMovieList);

        Movie movie = suggestMovieService.suggestRandomMovie(10, Arrays.asList(Genre.Action));
        assertEquals(true, mockMovieList.contains(movie));
    }

    @Test(expected = JsonParseException.class)
    public void testSuggestRandomMovieMethodWhenParseIsNotSuccessful() throws Exception {
        when(movieDbMapperService.mapToMovieList(anyString())).thenThrow(IOException.class);
        suggestMovieService.suggestRandomMovie(10, Arrays.asList(Genre.Comedy));
    }

    @Test
    public void testSuggestRandomMovieMethodWhenNotMoviesAreFound() throws Exception {
        Integer[] mockGenreIds = new Integer[1];
        mockGenreIds[0] = 28;
        when(movieDbMapperService.mapToMovieList(anyString())).thenReturn(Arrays.asList());

        Movie movie = suggestMovieService.suggestRandomMovie(10, Arrays.asList(Genre.Action));
        assertEquals(null, movie);
    }
}

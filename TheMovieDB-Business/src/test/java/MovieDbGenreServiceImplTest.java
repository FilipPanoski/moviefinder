import object.Genre;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import service.helper.MovieDbGenreService;
import service.helper.impl.MovieDbGenreServiceImpl;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
public class MovieDbGenreServiceImplTest {

    private MovieDbGenreService movieDbGenreService;

    @Before
    public void setUp() throws Exception {
        movieDbGenreService = new MovieDbGenreServiceImpl();
    }

    @Test
    public void testParseJsonGenreToGenreListMethodWithAllAvailableGenres() throws Exception {
        List<Genre> movieGenres = movieDbGenreService.parseJsonGenreToGenreList(Arrays.asList(28, 12, 35, 27, 878, 53));
        assertEquals(6, movieGenres.size());
    }

    @Test
    public void testParseJsonGenreToGenreListMethodWithSomeNotAvailableGenres() throws Exception {
        List<Genre> movieGenres = movieDbGenreService.parseJsonGenreToGenreList(Arrays.asList(28, 50, 3, 27, 878, 53));
        assertEquals(4, movieGenres.size());
    }
}

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import service.helper.MovieDbCalendarService;
import service.helper.impl.MovieDbCalendarServiceImpl;

import java.text.ParseException;
import java.util.Calendar;

@RunWith(SpringRunner.class)
public class MovieDbCalendarServiceImplTest {

    private MovieDbCalendarService movieDbCalendarService;

    @Before
    public void setUp() throws Exception {
        movieDbCalendarService = new MovieDbCalendarServiceImpl("yyyy-MM-DD");
    }

    @Test
    public void testParseStringDateToCalendarMethodWhenParseIsSuccessful() throws Exception {
        Calendar resultDate = movieDbCalendarService.parseStringDateToCalendar("2012-12-12");
        assertNotNull(resultDate);
    }

    @Test(expected = ParseException.class)
    public void testParseStringDateToCalendarMethodWhenParseIsNotSuccessful_ShouldThrowException() throws Exception {
        movieDbCalendarService.parseStringDateToCalendar("2012.21.12");
    }

    @Test
    public void testParseStringDateToCalendarMethodWhenMovieDoesNotHaveADate() throws Exception {
        Calendar resultDate = movieDbCalendarService.parseStringDateToCalendar("");
        assertEquals(null, resultDate);
    }
}

import static org.junit.Assert.*;

import object.Genre;
import object.Movie;
import org.bson.json.JsonParseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

import org.springframework.test.context.junit4.SpringRunner;
import service.MovieDbSearchServiceImpl;
import service.SearchMovieService;
import service.helper.MovieDbMapperService;

import java.io.IOException;
import java.util.*;

@RunWith(SpringRunner.class)
public class MovieDbSearchServiceImplTest {

    @Mock
    private MovieDbMapperService movieDbMapperService;
    private SearchMovieService searchMovieService;
    private String movieDbUrl;

    @Before
    public void setUp() throws Exception {
        movieDbUrl = "https://api.themoviedb.org/3/search/movie?api_key=964e1a2c4054adb70ac02a6842ca8a9c&query={name}";
        searchMovieService = new MovieDbSearchServiceImpl(movieDbUrl, movieDbMapperService);
    }

    @Test
    public void testSearchMoviesMethodWhenParseIsSuccessful() throws Exception {
        Movie mockMovie = new Movie("someName", Calendar.getInstance(),
                10, Arrays.asList(Genre.Comedy));
        when(movieDbMapperService.mapToMovieList(anyString()))
                .thenReturn(Arrays.asList(mockMovie));
        List<Movie> movieList = searchMovieService.searchMovies("someName", null);
        assertEquals(true, movieList.contains(mockMovie));
        assertEquals(1, movieList.size());
    }

    @Test(expected = JsonParseException.class)
    public void testSearchMoviesMethodWhenParseIsNotSuccessful_ShouldThrowException() throws Exception {
        Movie mockMovie = new Movie("someName", Calendar.getInstance(),
                10, Arrays.asList(Genre.Comedy));
        when(movieDbMapperService.mapToMovieList(anyString()))
                .thenThrow(new IOException());
        List<Movie> movieList = searchMovieService.searchMovies("someName", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSearchMoviesMethodWhenNameArgumentIsNotProvided_ShouldThrowException() throws Exception {
        Movie mockMovie = new Movie("someName", Calendar.getInstance(),
                10, Arrays.asList(Genre.Comedy));
        when(movieDbMapperService.mapToMovieList(anyString()))
                .thenReturn(Arrays.asList(mockMovie));
        List<Movie> movieList = searchMovieService.searchMovies(null, null);
    }
}

import exception.DateParseException;
import object.Genre;
import object.Movie;
import com.fasterxml.jackson.core.JsonParseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.springframework.test.context.junit4.SpringRunner;
import service.helper.MovieDbCalendarService;
import service.helper.MovieDbGenreService;
import service.helper.MovieDbMapperService;
import service.helper.impl.MovieDbMapperServiceImpl;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

@RunWith(SpringRunner.class)
public class MovieDbMapperServiceImplTest {

    @Mock
    private MovieDbCalendarService movieDbCalendarService;
    @Mock
    private MovieDbGenreService movieDbGenreService;
    private MovieDbMapperService movieDbMapperService;
    private String jsonString;

    @Before
    public void setUp() throws Exception {
        movieDbMapperService = new MovieDbMapperServiceImpl(movieDbCalendarService, movieDbGenreService);
        jsonString = "{\"page\":1,\"total_results\":2,\"total_pages\":1,\"results\":[{\"vote_count\":3307,\"id\":75780,\"video\":false,\"vote_average\":6.3,\"title\":\"Jack Reacher\",\"popularity\":14.627593,\"poster_path\":\"\\/38bmEXmuJuInLs9dwfgOGCHmZ7l.jpg\",\"original_language\":\"en\",\"original_title\":\"Jack Reacher\",\"genre_ids\":[80,18,53],\"backdrop_path\":\"\\/ezXodpP429qK0Av89pVNlaXWJkQ.jpg\",\"adult\":false,\"overview\":\"In an innocent heartland city, five are shot dead by an expert sniper. The police quickly identify and arrest the culprit, and build a slam-dunk case. But the accused man claims he's innocent and says \\\"Get Jack Reacher.\\\" Reacher himself sees the news report and turns up in the city. The defense is immensely relieved, but Reacher has come to bury the guy. Shocked at the accused's request, Reacher sets out to confirm for himself the absolute certainty of the man's guilt, but comes up with more than he bargained for.\",\"release_date\":\"2012-12-20\"},{\"vote_count\":2051,\"id\":343611,\"video\":false,\"vote_average\":5.3,\"title\":\"Jack Reacher: Never Go Back\",\"popularity\":15.265987,\"poster_path\":\"\\/IfB9hy4JH1eH6HEfIgIGORXi5h.jpg\",\"original_language\":\"en\",\"original_title\":\"Jack Reacher: Never Go Back\",\"genre_ids\":[28],\"backdrop_path\":\"\\/nDS8rddEK74HfAwCC5CoT6Cwzlt.jpg\",\"adult\":false,\"overview\":\"Jack Reacher must uncover the truth behind a major government conspiracy in order to clear his name. On the run as a fugitive from the law, Reacher uncovers a potential secret from his past that could change his life forever.\",\"release_date\":\"2016-10-19\"}]}";
    }

    @Test
    public void testMapToMovieListMethodWhenMappingIsSuccessful() throws Exception {
        when(movieDbCalendarService.parseStringDateToCalendar(anyString())).thenReturn(Calendar.getInstance());
        when(movieDbGenreService.parseJsonGenreToGenreList(anyList())).thenReturn(Arrays.asList(Genre.Action));
        List<Movie> movieList = movieDbMapperService.mapToMovieList(jsonString);
        assertEquals(2, movieList.size());
    }

    @Test(expected = JsonParseException.class)
    public void testMapToMovieListMethodWhenMappingIsNotSuccessful_ShouldThrowException() throws Exception {
        jsonString = "unParsable json format";
        when(movieDbCalendarService.parseStringDateToCalendar(anyString())).thenReturn(Calendar.getInstance());
        when(movieDbGenreService.parseJsonGenreToGenreList(anyList())).thenReturn(Arrays.asList(Genre.Action));
        movieDbMapperService.mapToMovieList(jsonString);
    }

    @Test(expected = DateParseException.class)
    public void testMapToMovieListMethodWhenCalendarIsNotParsed_ShouldThrowException() throws Exception {
        when(movieDbCalendarService.parseStringDateToCalendar(anyString())).thenThrow(ParseException.class);
        when(movieDbGenreService.parseJsonGenreToGenreList(anyList())).thenReturn(Arrays.asList(Genre.Action));
        movieDbMapperService.mapToMovieList(jsonString);
    }
}

import exception.FeatureNotSupportedException;
import object.Genre;
import object.Movie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import service.AddMovieService;
import service.MovieDbAddServiceImpl;

import java.util.Arrays;
import java.util.Calendar;

@RunWith(SpringRunner.class)
public class MovieDbAddServiceImplTest {

    private AddMovieService addMovieService;

    @Before
    public void setUp() throws Exception {
        addMovieService = new MovieDbAddServiceImpl();
    }

    @Test(expected = FeatureNotSupportedException.class)
    public void testAddNewMovieToDatabaseMethod_ShouldThrowException() throws Exception {
        addMovieService.addNewMovieToDatabase(new Movie("someName", Calendar.getInstance(),
                10, Arrays.asList(Genre.Comedy)), "12.12.2012");
    }
}

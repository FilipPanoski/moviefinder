package config;

import helper.MovieDatabaseQuery;
import helper.MovieDatabaseQueryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import repository.MovieRepository;

/**
 * Configures services that help with database query.
 */
@Configuration
public class IntegrationConfig {

    /**
     * @param movieRepository repository that manipulates with Movie.class objects.
     */
    @Bean
    public MovieDatabaseQuery movieDatabaseQuery(MovieRepository movieRepository) {
        return new MovieDatabaseQueryImpl(movieRepository);
    }
}

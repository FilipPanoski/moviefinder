package helper;

import object.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.MovieRepository;

import java.util.Calendar;

/**
 * Implementation of the MovieDatabaseQuery interface which queries Movie.class objects from a database.
 */
@Service
public class MovieDatabaseQueryImpl implements MovieDatabaseQuery {

    private MovieRepository movieRepository;

    @Autowired
    public MovieDatabaseQueryImpl(MovieRepository movieRepository){
        this.movieRepository = movieRepository;
    }

    /**
     * {@inheritDoc}
     */
    public Boolean findMovieInDbByNameAndDate(String movieName, Calendar releaseDate){
        Movie movie = movieRepository.findByNameAndReleaseDate(movieName, releaseDate);
        return (movie != null);
    }
}

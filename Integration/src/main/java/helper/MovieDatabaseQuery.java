package helper;

import java.util.Calendar;

/**
 * Service which queries Movie.class objects from a database.
 */
public interface MovieDatabaseQuery {

    /**
     * Looks for a particular movie in a database using the movieName argument as the name of the movie and
     * the releaseDate argument as the date when the movie was released.
     *
     * @param movieName the name of the movie
     * @param releaseDate the release date of the movie
     * @return Boolean which is true if the movie was found and false if it was not.
     */
    Boolean findMovieInDbByNameAndDate(String movieName, Calendar releaseDate);
}

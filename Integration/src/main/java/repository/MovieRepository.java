package repository;

import object.Genre;
import object.Movie;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.List;

@Repository
public interface MovieRepository extends MongoRepository<Movie, String> {

    Movie findByNameAndReleaseDate(String name, Calendar releaseDate);
    List<Movie> findByName(String name);
    List<Movie> findByGenre(List<Genre> genres);
    List<Movie> findByNameAndGenre(String name, List<Genre> genres);
    List<Movie> findByNameOrGenre(String name, List<Genre> genres);
    List<Movie> findByRatingAndGenre(Integer rating, List<Genre> genres);
    List<Movie> findByRatingOrGenre(Integer rating, List<Genre> genres);
    List<Movie> findByRating(Integer rating);
}

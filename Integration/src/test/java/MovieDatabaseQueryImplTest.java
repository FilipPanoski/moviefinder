import helper.MovieDatabaseQuery;
import helper.MovieDatabaseQueryImpl;
import static org.junit.Assert.*;

import object.Genre;
import object.Movie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

import org.springframework.test.context.junit4.SpringRunner;
import repository.MovieRepository;

import java.util.Arrays;
import java.util.Calendar;

@RunWith(SpringRunner.class)
public class MovieDatabaseQueryImplTest {

    @Mock
    private MovieRepository movieRepository;
    private MovieDatabaseQuery movieDatabaseQueryImpl;

    @Before
    public void setUp() throws Exception{
        movieDatabaseQueryImpl = new MovieDatabaseQueryImpl(movieRepository);
    }

    @Test
    public void testFindMovieInDbByNameAndDateMethodWhenMovieIsNotInDatabase() throws Exception {
        Calendar releaseDate = Calendar.getInstance();
        when(movieRepository.findByNameAndReleaseDate("mockName", releaseDate))
                .thenReturn(null);

        Boolean result = movieDatabaseQueryImpl.findMovieInDbByNameAndDate("mockName", releaseDate);
        assertEquals(false, result);
    }

    @Test
    public void testFindMovieInDbByNameAndDateMethodWhenMovieIsInDatabase() throws Exception {
        Calendar releaseDate = Calendar.getInstance();
        Movie mockMovie = new Movie("mockName", releaseDate, 10, Arrays.asList(Genre.Thriller));
        when(movieRepository.findByNameAndReleaseDate("mockName", releaseDate)).thenReturn(mockMovie);

        Boolean result = movieDatabaseQueryImpl.findMovieInDbByNameAndDate("mockName", releaseDate);
        assertEquals(true, result);
    }
}

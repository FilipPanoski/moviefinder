package object;

public enum Genre {
    Action,
    Adventure,
    SciFi,
    Horror,
    Thriller,
    Comedy
}
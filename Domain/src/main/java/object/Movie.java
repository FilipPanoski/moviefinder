package object;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Document(collection = "Movies")
@Data
public class Movie {

    @Id
    private String id;
    @Indexed
    private String name;
    private Calendar releaseDate;
    private Integer rating;
    private List<Genre> genre;

    public Movie() {
        this.genre = new ArrayList<Genre>();
    }

    public Movie(String id, String name, Calendar releaseDate, Integer rating, List<Genre> genre) {
        this.id = id;
        this.name = name;
        this.releaseDate = releaseDate;
        this.rating = rating;
        this.genre = genre;
    }

    public Movie(String name, Calendar releaseDate, Integer rating, List<Genre> genre) {
        this.name = name;
        this.releaseDate = releaseDate;
        this.rating = rating;
        this.genre = genre;
    }
}

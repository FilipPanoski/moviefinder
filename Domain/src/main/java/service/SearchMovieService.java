package service;

import object.Genre;
import object.Movie;

import java.util.List;

/**
 * Service which looks for movies in a database.
 */
public interface SearchMovieService {

    /**
     * Searches for all movies containing the movieName argument as the name of the movie and
     * the genres argument as the different genres of the movie.
     *
     * @param movieName the name of the movie being searched.
     * @param genres the genres of the movie.
     * @return a list of movies containing the corresponding arguments.
     */
    List<Movie> searchMovies(String movieName, List<Genre> genres);
}

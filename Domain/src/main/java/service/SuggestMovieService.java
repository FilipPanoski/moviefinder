package service;

import object.Genre;
import object.Movie;

import java.util.List;

/**
 * Service which suggests a random movie from a database.
 */
public interface SuggestMovieService {

    /**
     * Gets a random movie based on the corresponding arguments.
     *
     * @param movieRating the rating of the movie as criteria.
     * @param genres the genres of the movie as criteria.
     * @return random movie based on the criteria.
     */
    Movie suggestRandomMovie(Integer movieRating, List<Genre> genres);
}
package service;

import object.Movie;

/**
 * Service which adds new movies to a database.
 */
public interface AddMovieService {

    /**
     * Tries to add a new movie to the database using the movie argument as the movie and
     * the releaseDate argument as the release date of the movie.
     * Returns the movie if it was successfully added.
     *
     * @param movie the movie that needs to be added.
     * @param releaseDate the release date of the movie.
     * @return the movie that was successfully added with updated date release.
     */
    Movie addNewMovieToDatabase(Movie movie, String releaseDate);
}
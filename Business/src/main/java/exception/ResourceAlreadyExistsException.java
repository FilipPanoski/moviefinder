package exception;

/**
 * Exception that is thrown when the user tries to add a resource that
 * is already added to the database.
 */
public class ResourceAlreadyExistsException extends RuntimeException {

    /**
     * Generates an exception and prints the reason why it was thrown.
     *
     * @param message the message that is printed when the exception is thrown.
     */
    public ResourceAlreadyExistsException(String message) {
        super(message);
    }
}

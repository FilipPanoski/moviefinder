package exception;

/**
 * Exception that is thrown when user enters a date with an invalid format
 * which can not be parsed.
 */
public class IncorrectDateFormatException extends RuntimeException {

    /**
     * Generates an exception and prints the reason it was thrown and the original
     * exception that caused it.
     *
     * @param message the message that is printed when the exception is thrown.
     * @param ex      the original exception that caused this one to be thrown.
     */
    public IncorrectDateFormatException(String message, Throwable ex) {
        super(message, ex);
    }
}

package helper;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Service which manipulates with java.util.Calendar dates.
 */
public interface CalendarService {


    /**
     * Parses the String argument date into a java.util.Calendar date.
     * Throws an exception if the String can not be parsed.
     *
     * @param date the date that needs to be parsed, resembles a real date.
     * @return a parsed java.util.Calendar date.
     * @throws ParseException
     */
    Calendar parseStringDateToCalendar(String date) throws ParseException;
}

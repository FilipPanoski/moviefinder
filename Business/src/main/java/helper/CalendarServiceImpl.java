package helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Implementation of the CalendarService interface which helps with java.util.Calendar manipulation.
 */
@Component
public class CalendarServiceImpl implements CalendarService {

    private String dateFormat;

    @Autowired
    public CalendarServiceImpl(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    /**
     * {@inheritDoc}
     */
    public Calendar parseStringDateToCalendar(String date) throws ParseException{
        Calendar parsedDate = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        parsedDate.setTime(simpleDateFormat.parse(date));
        return parsedDate;
    }

}

package service;
import object.Genre;
import object.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.MovieRepository;

import java.util.List;
import java.util.Random;

/**
 * Implementation of the SuggestMovieService interface which suggests a random movie from a database.
 */
@Service
public class SuggestMovieServiceImpl implements SuggestMovieService {

    private MovieRepository movieRepository;

    @Autowired
    public SuggestMovieServiceImpl(MovieRepository movieRepository){
        this.movieRepository = movieRepository;
    }

    /**
     * {@inheritDoc}
     *
     * Returns null if no movie is found based on those arguments.
     */
    public Movie suggestRandomMovie(Integer movieRating, List<Genre> genres) {
        if (movieRating != null && genres != null) {
            return getRandomMovie(movieRepository.findByRatingAndGenre(movieRating, genres));
        } else {
            return getRandomMovie(movieRepository.findByRatingOrGenre(movieRating, genres));
        }
    }

    private Movie getRandomMovie(List<Movie> qualifiedMovies){
        if (qualifiedMovies.size() == 0) {
            return null;
        }
        int randomIndex = new Random().nextInt(qualifiedMovies.size());
        return qualifiedMovies.get(randomIndex);
    }
}

package service;

import exception.IncorrectDateFormatException;
import exception.ResourceAlreadyExistsException;
import helper.CalendarService;
import helper.MovieDatabaseQuery;
import object.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.MovieRepository;

import java.text.ParseException;
import java.util.Calendar;

/**
 * An implementation of the AddMovieService interface which adds new movies to a database.
 */
@Service
public class AddMovieServiceImpl implements AddMovieService {

    private MovieRepository movieRepository;
    private CalendarService calendarService;
    private MovieDatabaseQuery movieDatabaseQuery;

    @Autowired
    public AddMovieServiceImpl(MovieRepository movieRepository, CalendarService calendarService,
                               MovieDatabaseQuery movieDatabaseQuery){

        this.movieRepository = movieRepository;
        this.calendarService = calendarService;
        this.movieDatabaseQuery = movieDatabaseQuery;
    }

    /**
     * {@inheritDoc}
     *
     * Additionally tries to parse String releaseDate argument into a java.util.Calendar date.
     * Throws an IncorrectDateFormatException if fails to do so.
     *
     * If the movie that is being added already exists in the database then it
     * throws a ResourceAlreadyExistsException.
     */
    public Movie addNewMovieToDatabase(Movie movie, String releaseDate) {
        Calendar parsedDate = parseDate(releaseDate);
        Boolean isMovieAlreadyInDatabase = movieDatabaseQuery.findMovieInDbByNameAndDate(movie.getName(), parsedDate);
        if (isMovieAlreadyInDatabase){
            throw new ResourceAlreadyExistsException("Resource you are trying to add already exists in database");
        }
        movie.setReleaseDate(parsedDate);
        movieRepository.save(movie);
        return movie;
    }

    private Calendar parseDate(String releaseDate) {
        Calendar parsedDate;
        try {
            parsedDate = calendarService.parseStringDateToCalendar(releaseDate);
        } catch (ParseException ex) {
            throw new IncorrectDateFormatException("Date could not be parsed", ex);
        }
        return parsedDate;
    }
}

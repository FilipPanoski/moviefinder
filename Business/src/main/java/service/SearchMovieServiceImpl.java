package service;

import object.Genre;
import object.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.MovieRepository;

import java.util.List;

/**
 * Implementation of the SearchMovieService interface which looks for movies in a database.
 */
@Service
public class SearchMovieServiceImpl implements SearchMovieService {

    private MovieRepository movieRepository;

    @Autowired
    public SearchMovieServiceImpl(MovieRepository movieRepository){
        this.movieRepository = movieRepository;
    }

    /**
     * {@inheritDoc}
     *
     * Returns null if no movies are found.
     */
    public List<Movie> searchMovies(String movieName, List<Genre> genres) {
        if (movieName != null && genres != null) {
            return movieRepository.findByNameAndGenre(movieName, genres);
        } else {
            return movieRepository.findByNameOrGenre(movieName, genres);
        }
    }
}

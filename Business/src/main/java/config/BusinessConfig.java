package config;

import helper.CalendarService;
import helper.CalendarServiceImpl;
import helper.MovieDatabaseQuery;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import repository.MovieRepository;
import service.*;

import java.util.Calendar;

/**
 * Configures beans of all services in the Business module.
 */
@Profile("moviefinder")
@Configuration
public class BusinessConfig {

    /**
     * @param movieRepository repository which manipulates with Movie.class objects.
     * @param calendarService service which helps with java.util.Calendar manipulation.
     * @param movieDatabaseQuery service which queries Movie.class objects.
     */
    @Bean
    public AddMovieService addMovieService(MovieRepository movieRepository, CalendarService calendarService,
                                           MovieDatabaseQuery movieDatabaseQuery) {

        return new AddMovieServiceImpl(movieRepository, calendarService, movieDatabaseQuery);
    }

    /**
     * @param movieRepository repository which manipulates with Movie.class objects.
     */
    @Bean
    public SearchMovieService searchMovieService(MovieRepository movieRepository) {
        return new SearchMovieServiceImpl(movieRepository);
    }

    /**
     * @param movieRepository repository which manipulates with Movie.class objects.
     */
    @Bean
    public SuggestMovieService suggestMovieService(MovieRepository movieRepository) {
        return new SuggestMovieServiceImpl(movieRepository);
    }

    /**
     * @param dateFormat the required format for java.util.Calendar dates.
     */
    @Bean
    public CalendarService calendarService(@Value("${calendar.dateFormat}") String dateFormat) {
        return new CalendarServiceImpl(dateFormat);
    }
}

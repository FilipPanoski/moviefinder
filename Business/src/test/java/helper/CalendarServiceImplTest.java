package helper;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.util.Calendar;

@RunWith(SpringRunner.class)
public class CalendarServiceImplTest {

    private CalendarService calendarService;

    @Before
    public void setUp() throws Exception {
        calendarService = new CalendarServiceImpl("dd.mm.yyyy");
    }

    @Test
    public void testParseStringDateToCalendarMethodWhenStringIsValid() throws Exception {
        Calendar result = calendarService.parseStringDateToCalendar("12.12.2012");
        assertNotEquals(null, result);
    }

    @Test(expected = ParseException.class)
    public void testParseStringDateToCalendarMethodWhenStringIsNotValid_ShouldThrowException() throws Exception {
         calendarService.parseStringDateToCalendar("notvalid.Date");
    }
}

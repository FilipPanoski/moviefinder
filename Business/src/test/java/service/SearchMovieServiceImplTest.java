package service;

import static org.junit.Assert.*;
import object.Genre;
import object.Movie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.springframework.test.context.junit4.SpringRunner;
import repository.MovieRepository;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

@RunWith(SpringRunner.class)
public class SearchMovieServiceImplTest {

    @Mock
    private MovieRepository movieRepository;
    private SearchMovieService searchMovieService;

    @Before
    public void setUp() throws Exception {
        searchMovieService = new SearchMovieServiceImpl(movieRepository);
    }

    @Test
    public void testSearchMoviesMethodByName() throws Exception {
        Movie mockMovieFirst = new Movie("mockName", Calendar.getInstance(),
                10, Arrays.asList(Genre.SciFi));
        Movie mockMovieSecond = new Movie("mockName", Calendar.getInstance(),
                8, Arrays.asList(Genre.Thriller));
        when(movieRepository.findByNameOrGenre("mockName", null))
                .thenReturn(Arrays.asList(mockMovieFirst, mockMovieSecond));

        List<Movie> moviesList = searchMovieService.searchMovies("mockName", null);
        assertEquals(Arrays.asList(mockMovieFirst, mockMovieSecond), moviesList);
    }

    @Test
    public void testSearchMoviesByGenre() throws Exception {
        Movie mockMovieFirst = new Movie("mockNameF", Calendar.getInstance(),
                10, Arrays.asList(Genre.Action));
        Movie mockMovieSecond = new Movie("mockNameS", Calendar.getInstance(),
                8, Arrays.asList(Genre.Action));
        when(movieRepository.findByNameOrGenre(null, Arrays.asList(Genre.Action)))
                .thenReturn(Arrays.asList(mockMovieFirst, mockMovieSecond));

        List<Movie> moviesList = searchMovieService.searchMovies(null, Arrays.asList(Genre.Action));
        assertEquals(Arrays.asList(mockMovieFirst, mockMovieSecond), moviesList);
    }

    @Test
    public void testSearchMoviesByNameAndGenre() throws Exception {
        Movie mockMovieFirst = new Movie("mockName", Calendar.getInstance(),
                10, Arrays.asList(Genre.Action));
        Movie mockMovieSecond = new Movie("mockName", Calendar.getInstance(),
                8, Arrays.asList(Genre.Action));
        when(movieRepository.findByNameAndGenre("mockName", Arrays.asList(Genre.Action)))
                .thenReturn(Arrays.asList(mockMovieFirst, mockMovieSecond));

        List<Movie> moviesList = searchMovieService.searchMovies("mockName",
                Arrays.asList(Genre.Action));
        assertEquals(Arrays.asList(mockMovieFirst, mockMovieSecond), moviesList);
    }

    @Test
    public void testSearchMoviesWhenBothArgumentsAreNull() throws Exception {
               when(movieRepository.findByNameOrGenre(null, null)).thenReturn(null);

        List<Movie> moviesList = searchMovieService.searchMovies(null, null);
        assertEquals(null, moviesList);
    }

}

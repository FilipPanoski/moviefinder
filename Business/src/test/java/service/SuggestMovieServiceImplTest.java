package service;

import static org.junit.Assert.*;
import object.Genre;
import object.Movie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.springframework.test.context.junit4.SpringRunner;
import repository.MovieRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

@RunWith(SpringRunner.class)
public class SuggestMovieServiceImplTest {

    @Mock
    private MovieRepository movieRepository;
    private SuggestMovieService suggestMovieService;
    private List<Movie> movieList;

    @Before
    public void setUp() throws Exception {
        suggestMovieService = new SuggestMovieServiceImpl(movieRepository);
        movieList = new ArrayList<Movie>();
    }

    @Test
    public void testSuggestRandomMovieMethodByRating() throws Exception {
        Movie mockMovieFirst = new Movie("mockNameF", Calendar.getInstance(),
                6, Arrays.asList(Genre.Comedy));
        Movie mockMovieSecond = new Movie("mockNameS", Calendar.getInstance(),
                6, Arrays.asList(Genre.SciFi));
        movieList.add(mockMovieFirst);
        movieList.add(mockMovieSecond);
        when(movieRepository.findByRatingOrGenre(6, null)).thenReturn(movieList);

        Movie randomMovie = suggestMovieService.suggestRandomMovie(6, null);
        assertEquals(true, movieList.contains(randomMovie));
    }

    @Test
    public void testSuggestRandomMovieMethodByGenre() throws Exception {
        Movie mockMovieFirst = new Movie("mockNameF", Calendar.getInstance(),
                10, Arrays.asList(Genre.Adventure));
        Movie mockMovieSecond = new Movie("mockNameS", Calendar.getInstance(),
                7, Arrays.asList(Genre.Adventure));
        movieList.add(mockMovieFirst);
        movieList.add(mockMovieSecond);
        when(movieRepository.findByRatingOrGenre(null, Arrays.asList(Genre.Adventure))).thenReturn(movieList);

        Movie randomMovie = suggestMovieService.suggestRandomMovie(null, Arrays.asList(Genre.Adventure));
        assertEquals(true, movieList.contains(randomMovie));
    }

    @Test
    public void testSuggestRandomMovieMethodByRatingAndGenre() throws Exception {
        Movie mockMovieFirst = new Movie("mockNameF", Calendar.getInstance(),
                10, Arrays.asList(Genre.Comedy));
        Movie mockMovieSecond = new Movie("mockNameS", Calendar.getInstance(),
                10, Arrays.asList(Genre.Comedy));
        movieList.add(mockMovieFirst);
        movieList.add(mockMovieSecond);
        when(movieRepository.findByRatingAndGenre(10, Arrays.asList(Genre.Comedy))).thenReturn(movieList);

        Movie randomMovie = suggestMovieService.suggestRandomMovie(10, Arrays.asList(Genre.Comedy));
        assertEquals(true, movieList.contains(randomMovie));
    }

    @Test
    public void testSuggestRandomMovieMethodWhenBothArgumentsAreNull() throws Exception {
        Movie mockMovieFirst = new Movie("mockNameF", Calendar.getInstance(),
                10, Arrays.asList(Genre.Comedy));
        Movie mockMovieSecond = new Movie("mockNameS", Calendar.getInstance(),
                10, Arrays.asList(Genre.Comedy));
        movieList.add(mockMovieFirst);
        movieList.add(mockMovieSecond);
        when(movieRepository.findByRatingAndGenre(10, Arrays.asList(Genre.Comedy))).thenReturn(movieList);

        Movie randomMovie = suggestMovieService.suggestRandomMovie(null, null);
        assertEquals(null, randomMovie);
    }

    @Test
    public void testSuggestRandomMovieMethodWhenNoMoviesAreFound() throws Exception {
        when(movieRepository.findByRatingAndGenre(10, Arrays.asList(Genre.Comedy))).thenReturn(movieList);

        Movie randomMovie = suggestMovieService.suggestRandomMovie(10, Arrays.asList(Genre.Comedy));
        assertEquals(null, randomMovie);
    }
}

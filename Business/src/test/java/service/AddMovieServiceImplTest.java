package service;

import exception.IncorrectDateFormatException;
import exception.ResourceAlreadyExistsException;
import helper.CalendarService;
import helper.MovieDatabaseQuery;
import object.Genre;
import object.Movie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import org.springframework.test.context.junit4.SpringRunner;
import repository.MovieRepository;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;

@RunWith(SpringRunner.class)
public class AddMovieServiceImplTest {

    @Mock
    private MovieRepository movieRepository;
    @Mock
    private CalendarService calendarService;
    @Mock
    private MovieDatabaseQuery movieDatabaseQuery;

    private AddMovieService addMovieService;

    @Before
    public void setUp() throws Exception {
        addMovieService = new AddMovieServiceImpl(movieRepository, calendarService, movieDatabaseQuery);
    }

    @Test
    public void testAddNewMovieToDatabaseMethodWhenMovieIsNotInDatabase() throws Exception {
        Calendar releaseDate = Calendar.getInstance();
        when(calendarService.parseStringDateToCalendar("12.12.2012")).thenReturn(releaseDate);
        when(movieDatabaseQuery.findMovieInDbByNameAndDate("mockName", releaseDate))
                .thenReturn(false);

        Movie mockMovie = new Movie("mockName", Calendar.getInstance(),
                10, Arrays.asList(Genre.Comedy));
        Movie movie = addMovieService.addNewMovieToDatabase(mockMovie, "12.12.2012");
        assertEquals(mockMovie, movie);
    }

    @Test(expected = ResourceAlreadyExistsException.class)
    public void testAddNewMovieToDatabaseMethodWhenMovieIsInDatabase_ShouldThrowException() throws Exception {
        Calendar releaseDate = Calendar.getInstance();
        when(calendarService.parseStringDateToCalendar("12.12.2012")).thenReturn(releaseDate);
        when(movieDatabaseQuery.findMovieInDbByNameAndDate("mockName", releaseDate))
                .thenReturn(true);

        Movie mockMovie = new Movie("mockName", Calendar.getInstance(),
                10, Arrays.asList(Genre.Comedy));
        Movie movie = addMovieService.addNewMovieToDatabase(mockMovie, "12.12.2012");
    }

    @Test
    public void testAddNewMovieToDatabaseMethodWhenDateIsParsed() throws Exception {
        Calendar releaseDate = Calendar.getInstance();
        when(calendarService.parseStringDateToCalendar("12.12.2012")).thenReturn(releaseDate);
        when(movieDatabaseQuery.findMovieInDbByNameAndDate("mockName",
                releaseDate)).thenReturn(false);

        Movie mockMovie = new Movie("mockName", Calendar.getInstance(),
                10, Arrays.asList(Genre.Comedy));
        Movie movie = addMovieService.addNewMovieToDatabase(mockMovie, "12.12.2012");
        assertEquals(mockMovie, movie);
    }

    @Test(expected = IncorrectDateFormatException.class)
    public void testAddNewMovieToDatabaseMethodWhenDateIsNotParsed_ShouldThrowException() throws Exception {
        when(calendarService.parseStringDateToCalendar("notvalid.Date")).thenThrow(ParseException.class);
        when(movieDatabaseQuery.findMovieInDbByNameAndDate("mockName",
                Calendar.getInstance())).thenReturn(false);

        Movie mockMovie = new Movie("mockName", Calendar.getInstance(),
                10, Arrays.asList(Genre.Comedy));
        Movie movie = addMovieService.addNewMovieToDatabase(mockMovie, "notvalid.Date");
    }
}

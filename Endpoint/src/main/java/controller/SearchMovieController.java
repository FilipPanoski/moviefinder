package controller;

import object.Genre;
import object.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.SearchMovieService;

import java.util.List;

/**
 * Rest Controller that searches for movies in a database.
 */
@RestController
@CrossOrigin
@RequestMapping("/movies")
public class SearchMovieController {

    private SearchMovieService searchMovieService;

    @Autowired
    public SearchMovieController(SearchMovieService searchMovieService){
        this.searchMovieService = searchMovieService;
    }

    /**
     * Searches for movies in a database using SearchMovieService interface.
     * Searches differently depending on which arguments are presented.
     *
     * @param movieName the name of the movie the user is looking for.
     * @param genres the genres of the movie the user is looking for.
     * @return a list of all qualified movies based on the arguments.
     */
    @GetMapping
    public List<Movie> getMovies(@RequestParam(value = "name", required = false) String movieName,
                                 @RequestParam(value = "genre", required = false) List<Genre> genres) {

        return searchMovieService.searchMovies(movieName, genres);
    }
}

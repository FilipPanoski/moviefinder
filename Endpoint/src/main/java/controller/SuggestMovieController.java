package controller;

import object.Genre;
import object.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.SuggestMovieService;

import java.util.List;

/**
 * Rest Controller that gets a random movie from a database.
 */
@RestController
@CrossOrigin
@RequestMapping("/movies/suggestions")
public class SuggestMovieController {

    private SuggestMovieService suggestMovieService;

    @Autowired
    public SuggestMovieController(SuggestMovieService suggestMovieService){
        this.suggestMovieService = suggestMovieService;
    }

    /**
     * Gets a random movie from a database using the SuggestMovieService interface.
     * Searches for a movie differently depending on which arguments are presented.
     *
     * @param rating the rating of the movie the user wants to get.
     * @param genres the genres of the movie the user is wants to get.
     * @return a random movie based on the corresponding arguments.
     */
    @GetMapping
    public Movie getRandomMovie(@RequestParam(required = false) Integer rating,
                                @RequestParam(required = false) List<Genre> genres) {

        return suggestMovieService.suggestRandomMovie(rating, genres);
    }
}

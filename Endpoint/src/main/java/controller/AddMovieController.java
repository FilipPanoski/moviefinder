package controller;

import object.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.AddMovieService;

/**
 * Rest Controller that adds new movies to a database.
 */
@RestController
@CrossOrigin
@RequestMapping("/movies")
public class AddMovieController {

    private AddMovieService addMovieService;

    @Autowired
    public AddMovieController(AddMovieService addMovieService){
        this.addMovieService = addMovieService;
    }

    /**
     * Adds a new movie to a database using AddMovieService interface.
     *
     * @param movie the Movie.class object that needs to be added.
     * @param releaseDate the String representation of the release date of the movie.
     * @return the Movie that was successfully added.
     */
    @PostMapping
    public Movie addNewMovie(@RequestBody Movie movie, @RequestParam String releaseDate) {
       return addMovieService.addNewMovieToDatabase(movie, releaseDate);
    }
}

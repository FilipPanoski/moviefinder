package config;

import controller.AddMovieController;
import controller.SearchMovieController;
import controller.SuggestMovieController;
import helper.CalendarService;
import helper.MovieDatabaseQuery;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import repository.MovieRepository;
import service.AddMovieService;
import service.AddMovieServiceImpl;
import service.SearchMovieService;
import service.SuggestMovieService;

/**
 * Configures all controller beans.
 */
@Configuration
public class EndpointConfig {

    /**
     * @param addMovieService service which adds movies to a database.
     */
    @Bean
    public AddMovieController addMovieController(AddMovieService addMovieService) {

        return new AddMovieController(addMovieService);
    }

    /**
     * @param searchMovieService service which searches for movies from a database.
     */
    @Bean
    public SearchMovieController searchMovieController(SearchMovieService searchMovieService) {
        return new SearchMovieController(searchMovieService);
    }

    /**
     * @param suggestMovieService service which gets a random movie from a database.
     */
    @Bean
    public SuggestMovieController suggestMovieController(SuggestMovieService suggestMovieService) {
        return new SuggestMovieController(suggestMovieService);
    }
}

import controller.SearchMovieController;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import object.Genre;
import object.Movie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import service.SearchMovieService;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

@RunWith(SpringRunner.class)
public class SearchMovieControllerTest {

    @Mock
    private SearchMovieService searchMovieService;
    private SearchMovieController searchMovieController;

    @Before
    public void setUp() throws Exception {
        searchMovieController = new SearchMovieController(searchMovieService);
    }

    @Test
    public void testGetMoviesMethod() throws Exception {
        Movie mockMovie = new Movie("mockName", Calendar.getInstance(),
                10, Arrays.asList(Genre.SciFi));
        when(searchMovieService.searchMovies("mockName", Arrays.asList(Genre.SciFi)))
                .thenReturn(Arrays.asList(mockMovie));

        List<Movie> moviesList = searchMovieController.getMovies("mockName", Arrays.asList(Genre.SciFi));
        assertEquals(true, moviesList.contains(mockMovie));
        assertEquals(1, moviesList.size());
    }
}

import controller.AddMovieController;
import object.Genre;
import object.Movie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import org.springframework.test.context.junit4.SpringRunner;
import service.AddMovieService;

import java.util.Arrays;
import java.util.Calendar;

@RunWith(SpringRunner.class)
public class AddMovieControllerTest {

    @Mock
    private AddMovieService addMovieService;
    private AddMovieController addMovieController;

    @Before
    public void setUp() throws Exception {
        addMovieController = new AddMovieController(addMovieService);
    }

    @Test
    public void testAddNewMovieMethod() throws Exception {
        Movie mockMovie = new Movie("mockName", Calendar.getInstance(),
                10, Arrays.asList(Genre.Horror));
        when(addMovieService.addNewMovieToDatabase(mockMovie, "12.12.2012")).thenReturn(mockMovie);

        Movie movie = addMovieController.addNewMovie(mockMovie, "12.12.2012");
        assertEquals(mockMovie, movie);
    }
}

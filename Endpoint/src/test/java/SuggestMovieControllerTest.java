import controller.SuggestMovieController;
import object.Genre;
import object.Movie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import org.springframework.test.context.junit4.SpringRunner;
import service.SuggestMovieService;

import java.util.Arrays;
import java.util.Calendar;

@RunWith(SpringRunner.class)
public class SuggestMovieControllerTest {

    @Mock
    private SuggestMovieService suggestMovieService;
    private SuggestMovieController suggestMovieController;

    @Before
    public void setUp() throws Exception {
        suggestMovieController = new SuggestMovieController(suggestMovieService);
    }

    @Test
    public void testSuggestRandomMovieMethod() throws Exception {
        Movie mockMovie = new Movie("mockName", Calendar.getInstance(),
                10, Arrays.asList(Genre.Adventure));
        when(suggestMovieService.suggestRandomMovie(10, Arrays.asList(Genre.Adventure)))
                .thenReturn(mockMovie);

        Movie movie = suggestMovieController.getRandomMovie(10, Arrays.asList(Genre.Adventure));
        assertEquals(mockMovie, movie);
    }
}

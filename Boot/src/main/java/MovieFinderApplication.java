import config.BusinessConfig;
import config.EndpointConfig;
import config.IntegrationConfig;
import config.TheMovieDbBusinessConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackages = "repository")
@Import({BusinessConfig.class, EndpointConfig.class, IntegrationConfig.class, TheMovieDbBusinessConfig.class})
@ComponentScan(basePackages = {"config"})
public class MovieFinderApplication {

    public static void main(String[] args){
        SpringApplication.run(MovieFinderApplication.class, args);
    }
}
